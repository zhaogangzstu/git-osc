---
layout: post
title: "在家远程办公，你是不是穿着睡衣就开始了？"
---

<p>大多数公司本周基本是远程在家办公的状态，是不是早上起床蓬头垢面胡乱扒拉几口饭就开始坐在电脑前？</p>

<p><img src="https://static.oschina.net/uploads/img/202002/04092224_sVrV.jpg" /></p>

<p>比如这样算是好的了，更惨的还有</p>

<p><img src="https://static.oschina.net/uploads/img/202002/04092224_nx0b.jpg" /></p>

<p>最好的是这样这样这样</p>

<p><img alt="" height="586" src="https://static.oschina.net/uploads/space/2020/0204/084135_jGr8_12.jpg" width="824" /></p>

<p>还有这样</p>

<p><img alt="" height="266" src="https://static.oschina.net/uploads/space/2020/0204/084207_GxXt_12.jpg" width="702" /></p>

<p>不管怎样，请一定保持你的妆容，还有你身后的背景正常，要不，突然发起的视频会议，还如何是好呢？你眼睛上的眼屎，乱七八糟的头发（当然，红薯是不需要担心这一点的），疲惫的表情，关键你身后摄像头可及的地方可能拜访一本助你昨晚入眠的成人杂志（你胡说，中国哪有成人杂志）&nbsp;&mdash;&mdash; 想起来可真香！</p>

<p><strong>如果你不想被同事、领导嘲笑，请梳妆打扮，整理房间，端坐，打开电脑浏览器</strong></p>

<h1><strong>输入&nbsp;<a href="https://gitee.com">https://gitee.com</a>&nbsp; 开始远程办公！</strong></h1>

<p><strong>今天是2020年2月4日，我是码妞，今天确诊病例 20471 （数据来源：<a href="https://voice.baidu.com/act/newpneumonia/newpneumonia/?from=oschina">百度疫情实时大数据报告</a>）。</strong></p>
