---
layout: post
title: "重磅推出：Gitee 2019 年度数据报告"
---

<p>&gt;&gt;&gt;&nbsp;<a href="https://oschina.gitee.io/gitee-2019-annual-report/" target="_blank"><span style="color:#d35400">点击直达 Gitee 2019 年度数据报告</span></a></p>

<p>转眼 2020 已经到来，回望 2019，中国开源走过了无比精彩的一年。新年伊始，Gitee 2019 年度数据报告如期而至。</p>

<h3><span style="color:#3498db"><strong>2019 年开源趋势</strong></span></h3>

<p><strong>编程语言</strong></p>

<p><img alt="" height="521" src="https://static.oschina.net/uploads/space/2020/0108/102051_z3hK_3820517.png" width="400" /></p>

<ul>
	<li>
	<p><strong>Python&nbsp;</strong>在 2019 年上升一位，首次进入前三，反映出人工智能持续升温。<strong>C++&nbsp;</strong>占比上升两位，当前底层基础设施性质的项目以及区块链、比特币等新兴项目主要使用的还是 C/C++。</p>
	</li>
	<li>
	<p><strong>Golang</strong>首次进入前十，这与近年来云原生、容器化的火爆不无关系。</p>
	</li>
	<li>
	<p><strong>Android</strong>&nbsp;整体下跌主要受到了近两年爆发的大前端趋势的影响。大前端技术在 2019 年争相进入落地期，催生出各种跨端方案。这些技术使得移动开发不再局限于 Android 与 iOS 这样的特定端，直接用跨端框架开发&ldquo;小程序&rdquo;，成为了开发者更便捷的选择。</p>
	</li>
</ul>

<p><img height="516" src="https://static.oschina.net/uploads/space/2020/0108/102028_34Vm_3820517.png" width="600" />&nbsp; &nbsp; &nbsp; &nbsp;<img height="1" src="https://static.oschina.net/uploads/space/2020/0108/101623_nsG0_3820517.gif" width="1" />&nbsp;&nbsp; &nbsp;&nbsp;</p>

<ul>
	<li>
	<p>随着 Google Flutter 框架的火热，<strong>Dart</strong>&nbsp;语言受到广泛关注和使用，增速排名第一。2019年 Flutter 发布了迄今为止最重大的版本，一举成为多平台框架，支持移动、Web、桌面与嵌入式设备；而 Dart 也在与 C 的互操作、UI 构建和预编译本地可执行程序等特性上大幅改进。</p>
	</li>
	<li>
	<p>注重高效、安全与并行的系统级编程语言&nbsp;<strong>Rust</strong>&nbsp;受到越来越多开发者的关注和使用。虽然国内目前并没有太多出众的 Rust 落地实践，但它确实是一门极具潜力的语言，并且获得了微软、谷歌等大厂的大力支持，未来可期。</p>
	</li>
	<li>
	<p><strong>MATLAB</strong>&nbsp;已连续两年以超过 130% 的速度增长，MATLAB 应用范围非常广，包括信号和图像处理、通讯、控制系统设计、测试和测量、财务建模和分析以及计算生物学等众多应用领域。</p>
	</li>
</ul>

<p><img height="470" src="https://static.oschina.net/uploads/space/2020/0108/102200_b91X_3820517.png" width="600" /></p>

<ul>
	<li>
	<p>2019 年有许多特别小众、具有强烈特色的语言出现在 Gitee 上。除了广受欢迎的明星开源项目，我们也非常欣喜地看到一些相对小众、极客式的、具有无限潜力的项目和新尝试涌现。</p>
	</li>
</ul>

<p><strong>功能分布</strong></p>

<p><img height="479" src="https://static.oschina.net/uploads/space/2020/0108/102318_Qbsi_3820517.png" width="400" />&nbsp; &nbsp; &nbsp;</p>

<ul>
	<li>
	<p><strong>人工智能</strong>这一功能分类首次进入前十，与去年相比增长了 60%。关于 2019 年人工智能的持续升温，不得不提到 2019 年 TensorFlow 与 PyTorch 的演进，TensorFlow 发布了 2.0，PyTorch 发布了 1.0，各自都带来了极其强大的新特性，同时，基于这两大框架，各大厂商也相继开源了各自的人工智能方案。</p>
	</li>
	<li>
	<p>微信开发跌出前十。</p>
	</li>
	<li>
	<p><strong>其他</strong>开源类项目同比增长 52.11%，其中增速最快的三个子类别是<strong>硬件相关项目、物联网/边缘计算、图书/手册/教程</strong>。中国自主研发的物联网操作系统 RT-Thread 成功获得数百万美金的 A 轮投资，Raspberry Pi （树莓派）发布 4.0 版本，也让物联网、硬件项目持续受到开发者关注。</p>
	</li>
</ul>

<p><img height="263" src="https://static.oschina.net/uploads/space/2020/0108/102421_yqn2_3820517.png" width="400" />&nbsp; &nbsp;<img height="1" src="https://static.oschina.net/uploads/space/2020/0108/101623_GxHo_3820517.gif" width="1" />&nbsp;&nbsp; &nbsp; &nbsp;</p>

<ul>
	<li>
	<p><strong>后台管理框架</strong>的捐赠者人数占比超过了四分之一，捐赠者占比最多的项目的一大共同特点是：应用性强、开箱即用。</p>
	</li>
</ul>

<p><strong>最受欢迎开源项目</strong></p>

<p><img height="465" src="https://static.oschina.net/uploads/space/2020/0108/102509_gXG6_3820517.png" width="600" /></p>

<p><img height="373" src="https://static.oschina.net/uploads/space/2020/0108/102641_NETG_3820517.png" width="600" /></p>

<ul>
	<li>
	<p>2019 年最受欢迎开源项目第一位由后台管理系统&nbsp;<a href="https://gitee.com/y_project/RuoYi" target="_blank">RuoYi&nbsp;</a>获得，2019 年获得 Star 数突破六千。</p>
	</li>
	<li>
	<p>2019 年 8 月底开源的<a href="https://gitee.com/harmonyos/OpenArkCompiler" target="_blank">华为方舟编译器（OpenArkCompiler）</a>仅用四个月就获得新增 Star 数排名第二位。</p>
	</li>
	<li>
	<p>Java 互联网云快速开发框架&nbsp;<a href="https://gitee.com/JeeHuangBingGui/jeeSpringCloud" target="_blank">JeeSpringCloud</a>，由于项目代码简洁、注释丰富、上手容易等特点，在 2019 年也受到众多开发者喜爱。</p>
	</li>
</ul>

<p><strong>最有价值开源项目</strong></p>

<p>GVP (&nbsp;<strong>G</strong>itee Most&nbsp;<strong>V</strong>aluable&nbsp;<strong>P</strong>roject ) -&nbsp;<a href="https://gitee.com/gvp" target="_blank">Gitee 最有价值开源项目计划</a>，是经综合评定优选的开源项目展示平台，截止目前已有 221 个开源项目入选 GVP，其中，2019 年有 93 个开源项目入选。</p>

<p>Gitee 指数是 2018 年底推出的一项功能，从代码活跃度、社区活跃度、团队健康、流行趋势、影响力这 5 个维度对开源项目进行分析，以直观数值的形式来展示对一个开源项目各方位的指标综合度量。</p>

<p>在 2019 年 Gitee 指数排名前 50 的开源项目中，GVP 占据 41 个席位，Gitee 指数普遍超过 80 分。2019 年新入选 GVP 的 Gitee 指数排名 TOP 10 如下：</p>

<p><img height="311" src="https://static.oschina.net/uploads/space/2020/0108/102737_1BH5_3820517.png" width="700" /></p>

<p>GVP 贡献者分析如下：&nbsp;</p>

<p><img height="237" src="https://static.oschina.net/uploads/space/2020/0108/102810_qTkr_3820517.png" width="400" /></p>

<p><img height="288" src="https://static.oschina.net/uploads/space/2020/0108/102833_FRU8_3820517.png" width="400" /></p>

<ul>
	<li>
	<p>GVP 项目贡献者人数普遍较多，一方面反映出有更多开发者愿意为优秀项目贡献代码，成为社区的一员；另一方面也体现了运作良好的社区对项目健康成长的意义。</p>
	</li>
</ul>

<h3><span style="color:#3498db"><strong>2019 年开源亮点</strong></span></h3>

<p><strong>更多开源组织</strong></p>

<p><img height="411" src="https://static.oschina.net/uploads/space/2020/0108/103406_PG9r_3820517.png" width="600" />&nbsp; &nbsp; &nbsp;&nbsp;</p>

<ul>
	<li>
	<p>随着各大科技企业对开源的关注持续升温，越来越多优秀的开源组织陆续入驻 Gitee，如&nbsp;<a href="https://gitee.com/antv" target="_blank">AntV</a>、<a href="https://gitee.com/baidu" target="_blank">百度开源</a>、<a href="https://gitee.com/Tencent-BlueKing" target="_blank">蓝鲸智云</a>&nbsp;、<a href="https://gitee.com/dcloud" target="_blank">DCLOUD</a>、<a href="https://gitee.com/didiopensource" target="_blank">滴滴开源</a>、华为鸿蒙（<a href="https://gitee.com/harmonyos" target="_blank">HarmonyOS</a>）、<a href="https://gitee.com/opensci" target="_blank">科学大数据开源社区</a>、欧拉（<a href="https://gitee.com/openeuler" target="_blank">openEuler</a>）、<a href="https://gitee.com/pkuvcl" target="_blank">北京大学数字视频编解码技术国家工程实验室视频编码组</a>、蚂蚁金服&nbsp;<a href="https://gitee.com/sofastack" target="_blank">SOFASTACK</a>&nbsp;、腾讯&nbsp;<a href="https://gitee.com/tarscloud" target="_blank">TARS</a>&nbsp;与微众银行（<a href="https://gitee.com/webank" target="_blank">WeBank</a>）等。</p>
	</li>
</ul>

<p><strong>更强的规则意识</strong></p>

<p><img height="336" src="https://static.oschina.net/uploads/space/2020/0108/103057_SzRG_3820517.png" width="600" />&nbsp; &nbsp; &nbsp;&nbsp;</p>

<ul>
	<li>
	<p>开发者对开源许可证的关注和规则意识在逐渐加强，2019 年，Gitee 使用开源协议的仓库占比 提升至 72%。</p>
	</li>
	<li>
	<p>2019 年 8 月，Gitee 率先支持中国开源许可证 &mdash;&mdash; MulanPSL（木兰宽松许可证）。</p>
	</li>
</ul>

<p><strong>更好的协作方式</strong></p>

<p><img height="329" src="https://static.oschina.net/uploads/space/2020/0108/103153_0bJJ_3820517.png" width="400" />&nbsp; &nbsp; &nbsp; &nbsp;<img height="1" src="https://static.oschina.net/uploads/space/2020/0108/101623_6hnW_3820517.gif" width="1" />&nbsp;&nbsp; &nbsp; &nbsp;</p>

<ul>
	<li>
	<p>2019 年，很多人迈出了参与社区的第一步&mdash;&mdash;提了第一个 Issue/PR，更多人选择用 Issue 来反馈问题、用 PR 来参与代码贡献，以更好的方式参与开源协作。相信未来会有更多开发者以更&ldquo;开源&rdquo;的方式参与开源，参与项目和社区的成长，感受开源带来的便利。</p>
	</li>
</ul>

<p><strong>更多人回馈开源</strong></p>

<p><img height="164" src="https://static.oschina.net/uploads/space/2020/0108/103231_BMBj_3820517.png" width="600" /></p>

<p>也有人选择更&ldquo;直接&rdquo;的方式表达对开源项目的认可。</p>

<p>2016 年，Gitee 正式上线捐赠功能。</p>

<ul>
	<li>
	<p>2019 年，近 2000 位开发者第一次在 Gitee 捐赠开源项目，回馈开源。</p>
	</li>
</ul>

<p><span style="color:#3498db"><strong>感恩一路同行，2020 期待你带来更多精彩。</strong></span></p>

<p>&gt;&gt;&gt;&nbsp;<a href="https://oschina.gitee.io/gitee-2019-annual-report/" target="_blank"><span style="color:#d35400">点击直达 Gitee 2019 年度数据报告</span></a></p>
