---
layout: post
title: "坑爹代码 | 这个 unsigned 我看了半天才想明白"
---

<p>下面这段再简单不过的代码，你能猜出是什么结果吗？</p>

<pre>
<code class="language-cpp">int main()
{
    unsigned int  i = 10;
    for(i ; i &gt;=0; i--)
    {
        printf("@_@&lt;--&gt;@_@\n");
    }

    printf("end\n");
}</code></pre>

<p>别运行，猜就行了，然后移步下面链接发表评论：</p>

<p><a href="https://gitee.com/oschina/bullshit-codes/blob/master/C/%E6%AD%BB%E5%BE%AA%E7%8E%AF.md">https://gitee.com/oschina/bullshit-codes/blob/master/C/死循环.md</a></p>

<p>码云 6 周年，我们正在征集各种坑爹代码</p>

<p>详细的参与方法请看&nbsp;&nbsp;<a href="https://gitee.com/oschina/bullshit-codes">https://gitee.com/oschina/bullshit-codes</a></p>

<p>------ 分割线 ------</p>

<p>其他坑爹代码吐槽：</p>

<ul>
	<li><a href="https://www.oschina.net/news/107032/gitee-bullshit-codes-stringbuffer" target="_blank">坑爹代码&nbsp;| 这样使用 StringBuffer 的方法有什么坑？</a></li>
	<li><a href="https://www.oschina.net/news/107081/gitee-bad-code-one-line-code" target="_blank">坑爹代码&nbsp;| 你写过的最长的一行代码有多长？？？</a></li>
	<li><a href="https://www.oschina.net/news/107151/nesting-bad-code">坑爹代码 | 循环+条件判断，你最多能嵌套几层？</a></li>
	<li><a href="https://www.oschina.net/news/107213/gitee-bullshit-codes-optimize-for-future">坑爹代码 | 为了后期优化查询速度 ~ 颇有商业头脑！</a></li>
	<li><a href="https://www.oschina.net/news/107315/gitee-bad-code-exception">坑爹代码 | 你是如何被异常玩然后变成玩异常的？</a></li>
	<li><a href="https://www.oschina.net/news/107345/gitee-bullshit-amazing-stream">坑爹代码 | Stream 玩得最 6 的代码，看过的人都惊呆了</a></li>
	<li><a href="https://www.oschina.net/news/107411/github-bullshit-code-with-go-defer">坑爹代码 | Go 语言的 defer 能制造出多少坑来？</a></li>
	<li><a href="https://www.oschina.net/news/107561/gitee-bullshit-code-java-logging">坑爹代码 | 这样的日志封装到底是坑爹还是有用呢？</a></li>
	<li><a href="https://www.oschina.net/news/107719/gitee-bullshit-code-phone-validate">坑爹代码 | 要不然你说手机号码应该怎么验证嘛！</a></li>
</ul>
